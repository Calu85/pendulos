#!/usr/bin/python
# -*- coding: utf-8 -*-

from pint import UnitRegistry
import numpy as np
#~ import matplotlib.pyplot as plt
import pyqtgraph as pg
unidad=UnitRegistry()

#~ Parámetros que debo elegir yo
#~ max_height=50 * unidad.cm
min_frec=51 * unidad.rpm
#~ diam_bocha=6.86 * unidad.cm
diam_bocha=4.267 * unidad.cm
delta_frec=1 * unidad.rpm
nro_bochas=15
dist_bochas=4.4 * unidad.cm



g=unidad.gravity
g=g.to(unidad.cm/unidad.min**2)

max_height=g/(min_frec**2)
#~ Parámetros derivados de los requisitos anteriores
largo_total=dist_bochas * nro_bochas
#~ Compruebo parámetros geométricos
dist_apoyos=dist_bochas

frecuencias=np.zeros(nro_bochas) * unidad.rpm
ubicaciones=np.linspace(0,largo_total.magnitude,num=nro_bochas)
min_frec=np.sqrt(g/max_height)

for i in range(nro_bochas):
    frecuencias[i]=min_frec+i*delta_frec

angulo_inicial=10 * unidad.deg

longitud_pendulos=(g/(frecuencias**2)).to(unidad.cm)

#~ longitud_hilo=2*np.sqrt(length**2+dist_bochas**2/4)

#~ hilo_necesario=np.sum(longitud_hilo)

#~ Calcular la maxima amplitud, que es la del péndulo más largo.
#~ maxima_amplitud

#~ calcular la frecuencia del video. Eligo que sea 4 veces la máxima frecuencia.
frec_video=4*frecuencias[frecuencias.magnitude.size-1]
T_video=1/frec_video

#~ ventana2D = pg.GraphicsWindow(title="Vista frontal del péndulo")
#~ ventana2D.resize(1000,600)
#~ ventana2D.setWindowTitle('Vista frontal')
#~ 
#~ vistaFrontal = ventana2D.addPlot(title="Vista frontal")
#~ vistaFrontal.plot(ubicaciones, longitud_pendulos, pen=None, symbol='o', symbolBrush=(0,250,0), symbolPen='w', symbolSize=40)
#~ 
#~ vistaLateral = ventana2D.addPlot(title="Vista lateral")
#~ vistaLateral.plot(np.zeros(nro_bochas), longitud_pendulos, pen=None, symbol='o', symbolBrush=(0,250,0), symbolPen='w', symbolSize=40)

#~ ventana2D.nextRow()
#~ vistaSuperior = ventana2D.addPlot(title="Vista superior")
#~ vistaSuperior.plot(ubicaciones,np.zeros(nro_bochas) , pen=None, symbol='o', symbolBrush=(0,250,0), symbolPen='w', symbolSize=40)


ventanaAnim = pg.GraphicsWindow()

ventanaAnim.setWindowTitle('Vista frontal')
ventanaAnim.resize(1000,1000)
punto1 = ventanaAnim.addPlot()

curva1 = punto1.plot(pen='c', symbol='o', symbolBrush=(0,250,0), symbolPen='w', symbolSize=40)

t = 0.0 * unidad.min

def update():
    global curva1, t, frecuencias, angulo_inicial, longitud_pendulos
    theta=(angulo_inicial.to(unidad.rad))*np.cos(frecuencias*t)
    t+=T_video
    x=longitud_pendulos.magnitude*np.sin(theta)
    y=longitud_pendulos.magnitude*np.cos(theta)
    curva1.setData(x,y)

timer = pg.QtCore.QTimer()
timer.timeout.connect(update)
timer.start(50)    
